<?php

class PrincetonPrinterDataObject extends KGODataObject
{
    const PRINTER_DESCRIPTION_ATTRIBUTE = 'pu:description';
    const SNIPPET_ATTRIBUTE = 'pu:Snippet';

    public function getSnippet() {
        return $this->getAttribute(self::SNIPPET_ATTRIBUTE);
    }

    public function getDescription() {
        return $this->getAttribute(self::PRINTER_DESCRIPTION_ATTRIBUTE);
    }


    public function getUIField(KGOUIObject $object, $field) {
        //kgo_debug($field,false,false);       
        switch ($field) {     
            case 'title':
                //$snippet=$this->getSnippet();
                $description=$this->getDescription();
                #return "$snippet $description";
                $desc_parts=explode("<br>",$description);
                $building=$desc_parts[0];
                if($building == "") {
                    $building="No Building";
                } else {
                    $building=str_replace("Building: ","",$building);
                }
                $room=$desc_parts[1];
                if($room == "") {
                    $room="No Room";
                } else {
                    $room=str_replace(" Room","",$room);
                }
                return "$building $room";
            case 'subtitle':
                $description=$this->getDescription();
                $desc_parts=explode("<br>",$description);
                $windows_info=$desc_parts[2];
                if(preg_match("/window/i",$windows_info) == 0) {
                    $windows="No Windows Info";
                }
                $mac_info=$desc_parts[3];
                if(preg_match("/mac/i",$mac_info) == 0) {
                    $mac_info="No Macs Info";
                }
                $printer_info=$desc_parts[4];
                if($printer_info == "") {
                    $printer_info="No Printer Info";
                }
                $tmp_array = array($windows_info, $mac_info, $printer_info);
                $tmp_info=implode("</br >",$tmp_array);
                return $tmp_info;
            default:
                return parent::getUIField($object, $field);
        }
    }
}
