<?php


class PrincetonPrinterModule extends KGOModule {

    /*
     *  The initializeForPageConfigObjects_ methods below don't need to do much, they simply check if a feed has been configured
     *  The $objects configured in the page objdefs will take control from here
     */

    protected function initializeForPageConfigObjects_index(KGOUIPage $page, $objects) {
        $jc_feeds=$this->getFeed();
        //kgo_debug($jc_feeds,true,true);
        //kgo_debug($this,true,true);
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, "no feed for printer-index");
            return;
        }
    }
    
    
    public function getPUPrinters() {
        //$tics=time();
        //kgo_debug("getPULaundryRooms called at tics[$tics]",true,true);    
        if($feed = $this->getFeed()) {
            $jc_retriever=$feed->getRetriever();
            //kgo_debug($jc_retriever,true,true);
            $jc_data=$jc_retriever->getData();
            //kgo_debug($jc_data,true,true);            
            if(is_array($jc_data)) {
                //kgo_debug($jc_data,true,true);
                return $jc_data;
            } else {
                $this->setPageError($page,"retriever data is not an array");
                return;
            }
        } else {
            $this->setPageError($page, "no feed for function getPUPrinters");
            return;        
        }
    }
    
}
